import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    public static void main(String[] args) {
        int zakres = 0;
        try{
            if(args.length == 1){
                zakres = Integer.parseInt(args[0]);
                if(zakres < 0){
                    System.out.println("Podana liczba musi być liczbą dodatnią");
                    System.out.println("Podaj w argumencie liczbę N oznaczającą zakres <0,N> w którym losowana będzie liczba");
                    System.exit(0);
                }
            }
            else if(args.length > 1){
                System.out.println("Podano za dużą liczbę argumentów");
                System.exit(0);
            }
            else{
                System.out.println("Podaj w argumencie liczbę N oznaczającą zakres <0,N> w którym losowana będzie liczba");
                System.exit(0);
            }
        }
        catch(NumberFormatException ex){
            System.out.println("Błąd konwersji string -> INT: " + ex.getMessage() + " [Wprowadzone dane muszą być liczbami całkowitymi]");
            System.exit(0);
        }
        Game game = new Game(zakres);
        game.start();
    }
}
