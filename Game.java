import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

class Game {
    Scanner input = new Scanner(System.in);
     private int number = 0;
     private int givenNumber = 0;
     private int numberOfShots = 0;
     Game(int number){
         givenNumber = number;
     }
     void start(){
         setNumber();
         lore();
         interact();
     }
     private void lore(){
        System.out.println("Jest tak - podales w argumencie liczbe " + givenNumber + " a ja wylosowałem liczbe" );
        System.out.println("z zakresu <0,twojaLiczba> czyli <0," + givenNumber + ">");
        System.out.println("no i ten... ty piszesz liczbe, a ja mówie czy jest wieksza czy mniejsza od wylosowanej");
        System.out.println("No to co? do dzieła!");
    }
     private void setNumber(){
        number = ThreadLocalRandom.current().nextInt(0, givenNumber + 1);
    }
     private void interact(){

        boolean playing = true;
        do{
            System.out.print("Podaj liczbę całkowitą: ");
            long a;
            try{
                numberOfShots++;
                a = input.nextInt();

                if(a<0){
                    System.out.println("Podałeś liczbę mniejszą od zera!\nPodaj liczbę z zakresu <0, "+givenNumber+">");
                }
                else if(a > number){
                    System.out.println("Wylosowana liczba jest MNIEJSZA");
                }
                else if(a < number){
                    System.out.println("Wylosowana liczba jest WIEKSZA");
                }
                else{
                    System.out.println("WOW! Zgadles!!!");
                    System.out.println("Prob: " + numberOfShots);
                    input.nextLine();
                    playing = restart();
                }
            }
            catch(InputMismatchException ex){
                System.out.println("Podaj liczbe");
                System.out.println("liczbe z zakresu <0," + givenNumber + ">");
                input.next();
            }
        }while(playing);
    }
    private boolean decision(String description){
         do {
             System.out.print(description + " [T/N]:");
             String decision = input.nextLine();
             if(decision.equals("T") || decision.equals("t")){
                 return true;
             }
             else if(decision.equals("N") || decision.equals("n")){
                 return false;
             }
         }while (true);
    }
     private boolean restart() throws InputMismatchException{
        if(!decision("Grasz dalej?")){
            System.out.println("Dzieki za gre!");
            System.exit(0);
        }
        System.out.println("No to do dzieła!");
        setNumber();
        numberOfShots = 0;
        return true;
    }

}